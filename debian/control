Source: bing-ip2hosts
Section: utils
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Devon Kearns <dookie@kali.org>,
           Sophie Brun <sophie@offensive-security.com>,
Build-Depends: debhelper-compat (= 12)
Standards-Version: 4.6.2
Homepage: https://www.morningstarsecurity.com/research/bing-ip2hosts
Vcs-Git: https://gitlab.com/kalilinux/packages/bing-ip2hosts.git
Vcs-Browser: https://gitlab.com/kalilinux/packages/bing-ip2hosts

Package: bing-ip2hosts
Architecture: all
Depends: ${misc:Depends}, wget, bind9-dnsutils
Description: Enumerate hostnames for an IP using bing.com
 This package contains a Bing.com web scraper that discovers hostnames by IP
 address. Bing is the flagship Microsoft search engine formerly known as MSN
 Search and Live Search.
 .
 It provides a feature unique to search engines - it allows searching by IP
 address. Bing-ip2hosts uses this feature.
 .
 It can be used to discover subdomains and other related domains. It also helps
 to identify websites hosted in a shared hosting environment. This technique
 follows best practices during the reconnaissance phase of a penetration test
 or bug bounty, to expand the target's attack surface.
 .
 Unlike other many other recon tools that web scrape Bing, this tool has smart
 scraping behaviour to maximize the discovery of hostnames.
